$(document).ready(function(){

    $(".circle-nav").css("opacity", 1);
    $(".circle-nav-logo").velocity("fadeIn", 200);

    if( $(window).width() > 820 ) {
        distributeFields();
    }

	$(".circle-nav-item").on("click", function(e){

        if( $('.active').length != 0 ) {
            ga('send', {
                'hitType': 'event',
                'eventCategory': 'menu',
                'eventAction': 'close',
                'eventLabel': $(".active").children("p").text().trim(),
            });
        }

        if ( $(e.target).closest('.close').length === 0 ) {
                $(".circle-nav-item").removeClass("active");
            $(this).siblings(".circle-nav-item").addClass("background");
            $(this).addClass("active").removeClass("background");
            $(this).find(".circle-nav-item-child-item a").each(function() {
                $(this).velocity({
                    scale: .8
                }, "easeOut", "fast");
            });
            $(".circle-nav-item:not(.active) .circle-nav-item-child-item a").velocity({
                scale: 1.2
            }, "easeOut");

            ga('send', {
                'hitType': 'event',
                'eventCategory': 'menu',
                'eventAction': 'open',
                'eventLabel': $(".active").children("p").text().trim(),
            });
        }
	});

    $(".close").on("click", function(e){
        ga('send', {
            'hitType': 'event',
            'eventCategory': 'menu',
            'eventAction': 'close',
            'eventLabel': $(".active").children("p").text().trim(),
        });
        $(".active").removeClass("active");
        $(".circle-nav-item").removeClass("background");
        $(".circle-nav-item-child-item a").velocity({
            scale: 1.2
        }, "easeOut");
    });   

	$("body").on("click", function(e){
        if ( $(e.target).closest('.circle-nav-item').length === 0 ) {
            ga('send', {
                'hitType': 'event',
                'eventCategory': 'menu',
                'eventAction': 'close',
                'eventLabel': $(".active").children("p").text().trim(),
            });
            $(".active").removeClass("active");
            $(".circle-nav-item").removeClass("background");
            $(".circle-nav-item-child-item a").velocity({
                scale: 1.2
            }, "easeOut");
        } 
	});

    $('.circle-nav-item-child-item a').click(function(e){
        e.stopPropagation();
        ga('send', {
            'hitType': 'event',
            'eventCategory': 'link',
            'eventAction': 'exit_page',
            'eventLabel': $(this).closest(".circle-nav-item").children('p').text() + " >> " + $(this).text().trim(),
        });
    });

});

$(window).resize(function(){
    if( $(window).width() > 820 ) {
        $(".circle-nav-logo").css("opacity", 1)
        distributeFields();
    } else {
        $('.circle-nav-logo, .circle-nav-item, .circle-nav-item-child a').attr("style", "");
    }
});

function distributeFields() {
    var xvar = 2;

    var fields = $('.circle-nav-item');
    var container = $('.circle-nav');
    var width = container.width();
    var height = container.height();

    if( width > 725 ) {
		var radius = (width/4);
    } else {
		var radius = 200;
    }

    var angle = -2;
    var step = (xvar*Math.PI) / fields.length;

    $('.circle-nav-logo, .circle-nav-item').css({
		left: width/xvar - 90,
		top: height/xvar - 100,
    });

    fields.each(function() {
        var x = Math.round(width/xvar + radius * Math.cos(angle) - $(this).width()/xvar);
        var y = Math.round(height/xvar + radius * Math.sin(angle) - $(this).height()/xvar);
        var _this = $(this);
        _this.velocity({
            left: [x + 'px', "easeInOut"],
            top: [y + 'px', "spring"],
            opacity: "1",
        }, "easeOut");
        angle += step;
	    _this.find(".circle-nav-item-child a").each(function() {
	    	distributeChildMenus(_this);
	    });
    });
}


function distributeChildMenus(container) {
    var xvar = 2;

    var fields = container.find('.circle-nav-item-child-item a');
    var width = container.width();
    var height = container.height();
	var radius = width + 20;

    var angle = 0;
    var step = (xvar*Math.PI) / fields.length;

    fields.each(function() {
        var x = Math.round(width/xvar + radius * Math.cos(angle) - $(this).width()/xvar);
        var y = Math.round(height/xvar + radius * Math.sin(angle) - $(this).height()/xvar);
        $(this).velocity({
            left: [x + 'px', "easeInOut"],
            top: [y + 'px', "spring"],
            opacity: "1",
        }, { duration: 0 });
        angle += step;
    });
}

